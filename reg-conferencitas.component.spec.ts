import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegConferencitasComponent } from './reg-conferencitas.component';

describe('RegConferencitasComponent', () => {
  let component: RegConferencitasComponent;
  let fixture: ComponentFixture<RegConferencitasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegConferencitasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegConferencitasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
